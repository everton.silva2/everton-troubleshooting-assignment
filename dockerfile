FROM docker.elastic.co/beats/filebeat:7.14.0

USER root

RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash - && yum install -y nodejs

COPY package.json package-lock.json ./

RUN npm install

COPY . .

RUN chown root:filebeat filebeat.yml
USER filebeat

RUN ./filebeat modules enable elasticsearch

USER root

EXPOSE 3000

CMD npm run start && ./filebeat