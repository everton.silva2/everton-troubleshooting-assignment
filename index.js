const express  = require('express');
const logger = require('./logger.js');
// const Inspector = require('inspector-api')

const app = express();
const PORT = 3000;

var apm = require('elastic-apm-node').start({
    secretToken: '030BeZquHoy6CrajNQ',
    serverUrl: 'https://492cb05825be4780a5114f21ae92e4b2.apm.eastus2.azure.elastic-cloud.com:443',
    environment: 'development'
})

// inspector = new Inspector({
//     storage: {
//         type: 'fs'
//     }
// });

// inspector.profiler.enable().then(async () => await inspector.profiler.start());
    

app.listen(PORT, function(){
    console.log(`Running on port ${ PORT }`);
});

app.use((req, _, next) => {
    logger.log({
        level: 'info',
        message: `${ req.method } on URL: ${ req.url }`,
        header: req.headers,
        ip: req.ip,
        hostname: req.hostname,
        query: req.query,
        body: req.body
    });

    return next();
})

app.get('/', async (_, res) => {
    res.send({
        message: 'Sucessfull get'
    });
});

app.get('/error', async (req, res) => {
    const errorMessage = 'ERROR: ....';

    logger.log({
        level: 'error',
        message: `An error has occurred while ${ req.method } on ${ req.url } \n Error message: ${ errorMessage }`,
        header: req.headers,
        ip: req.ip,
        hostname: req.hostname,
        query: req.query,
        body: req.body
    });

    return res.send({
        message: 'Error on get'
    });
});


// process.on('SIGINT', async() => {
//     await inspector.profiler.stop();
//     process.exit(2);
// });


