const winston = require('winston');
 
const logger = winston.createLogger({
  level: 'info',
  format: winston.format.combine(
      winston.format.json(),
      winston.format.timestamp(),
      winston.format.metadata()
  ),
  transports: [
    new winston.transports.File({ filename: 'logs/logs.log' }),
  ],
});

if (process.env.NODE_ENV !== 'production') {
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

module.exports = logger;